+++
title = "Sponsoring Package Details"
description = "Want to support the R Stats Language for Statistical Computing and its world class community? Learn how to help and benefit from the reach of the useR! conference."
keywords = ["conference","sponsor","support","open source"]
+++

<style>

#jcznocsaql .gt_table {
  display: table;
  border-collapse: collapse;
  margin-left: auto;
  margin-right: auto;
  color: #333333;
  font-size: 12px;
  background-color: #FFFFFF;
  width: auto;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #A8A8A8;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #A8A8A8;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
}

#jcznocsaql .gt_heading {
  background-color: #FFFFFF;
  text-align: center;
  border-bottom-color: #FFFFFF;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
}

#jcznocsaql .gt_title {
  color: #333333;
  font-size: 125%;
  font-weight: initial;
  padding-top: 4px;
  padding-bottom: 4px;
  border-bottom-color: #FFFFFF;
  border-bottom-width: 0;
}

#jcznocsaql .gt_subtitle {
  color: #333333;
  font-size: 85%;
  font-weight: initial;
  padding-top: 0;
  padding-bottom: 4px;
  border-top-color: #FFFFFF;
  border-top-width: 0;
}

#jcznocsaql .gt_bottom_border {
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#jcznocsaql .gt_col_headings {
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
}

#jcznocsaql .gt_col_heading {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: normal;
  text-transform: inherit;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: bottom;
  padding-top: 5px;
  padding-bottom: 6px;
  padding-left: 5px;
  padding-right: 5px;
  overflow-x: hidden;
}

#jcznocsaql .gt_column_spanner_outer {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: normal;
  text-transform: inherit;
  padding-top: 0;
  padding-bottom: 0;
  padding-left: 4px;
  padding-right: 4px;
}

#jcznocsaql .gt_column_spanner_outer:first-child {
  padding-left: 0;
}

#jcznocsaql .gt_column_spanner_outer:last-child {
  padding-right: 0;
}

#jcznocsaql .gt_column_spanner {
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  vertical-align: bottom;
  padding-top: 5px;
  padding-bottom: 6px;
  overflow-x: hidden;
  display: inline-block;
  width: 100%;
}

#jcznocsaql .gt_group_heading {
  padding: 8px;
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: middle;
}

#jcznocsaql .gt_empty_group_heading {
  padding: 0.5px;
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  vertical-align: middle;
}

#jcznocsaql .gt_striped {
  background-color: rgba(128, 128, 128, 0.05);
}

#jcznocsaql .gt_from_md > :first-child {
  margin-top: 0;
}

#jcznocsaql .gt_from_md > :last-child {
  margin-bottom: 0;
}

#jcznocsaql .gt_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  margin: 10px;
  border-top-style: solid;
  border-top-width: 1px;
  border-top-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: middle;
  overflow-x: hidden;
}

#jcznocsaql .gt_stub {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-right-style: solid;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  padding-left: 12px;
}

#jcznocsaql .gt_summary_row {
  color: #333333;
  background-color: #FFFFFF;
  text-transform: inherit;
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
}

#jcznocsaql .gt_first_summary_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
}

#jcznocsaql .gt_grand_summary_row {
  color: #333333;
  background-color: #FFFFFF;
  text-transform: inherit;
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
}

#jcznocsaql .gt_first_grand_summary_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  border-top-style: double;
  border-top-width: 6px;
  border-top-color: #D3D3D3;
}

#jcznocsaql .gt_table_body {
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#jcznocsaql .gt_footnotes {
  color: #333333;
  background-color: #FFFFFF;
  border-bottom-style: none;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
}

#jcznocsaql .gt_footnote {
  margin: 0px;
  font-size: 90%;
  padding: 4px;
}

#jcznocsaql .gt_sourcenotes {
  color: #333333;
  background-color: #FFFFFF;
  border-bottom-style: none;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
}

#jcznocsaql .gt_sourcenote {
  font-size: 90%;
  padding: 4px;
}

#jcznocsaql .gt_left {
  text-align: left;
}

#jcznocsaql .gt_center {
  text-align: center;
}

#jcznocsaql .gt_right {
  text-align: right;
  font-variant-numeric: tabular-nums;
}

#jcznocsaql .gt_font_normal {
  font-weight: normal;
}

#jcznocsaql .gt_font_bold {
  font-weight: bold;
}

#jcznocsaql .gt_font_italic {
  font-style: italic;
}

#jcznocsaql .gt_super {
  font-size: 65%;
}

#jcznocsaql .gt_footnote_marks {
  font-style: italic;
  font-size: 65%;
}
</style>


<p>Sponsorship prices are adapted to the country of your company’s headquarters. The cost conversion was done according to Gross Domestic Product (GDP) adjusted by Purchasing Power Parity (PPP) provided by the World Bank. We want companies from different parts of the world to be able to sponsor useR! and believe that this is a fair approach. It also reflects the fact that the share of attendees from “High income” countries is higher and thus the price per person is different. The categories are listed in the table below.</p>
<p>You can read more about purchasing power parities, price level indexes, and PPP-based expenditures in the May 2020 World Bank post <a href="https://blogs.worldbank.org/opendata/new-results-international-comparison-program-shed-light-size-global-economy" target="_blank">New results from the International Comparison Program shed light on the size of the global economy.</a></p>

<p>The Global Income Groups listed in the table below were obtained using data from the 2017 International Comparison Program (ICP) which you can read more about in the report <a href="https://openknowledge.worldbank.org/bitstream/handle/10986/33623/9781464815300.pdf">Purchasing Power Parities and the Size of World Economies: Results from the 2017 International Comparison Program</a>. The conversion factors were calculated using PPP-based GDP per capita for each Global Income Group using data from the <a href="https://databank.worldbank.org/source/icp-2017">ICP 2017 World Bank Database</a>.</p>
<p><strong>Note:</strong> “Lower middle income” and “Low income” countries are grouped into the same pricing category for sponsoring. Please get in touch with us if your company is from a “Low income” country and this grouping might prevent the deal. <br><br></p>

<div id="jcznocsaql" style="overflow-x:auto;overflow-y:auto;width:auto;height:auto;"><table class="gt_table">
  <thead class="gt_header">
    <tr>
      <th colspan="7" class="gt_heading gt_title gt_font_normal" style>Cost Conversions by Global Income Group</th>
    </tr>
    <tr>
      <th colspan="7" class="gt_heading gt_subtitle gt_font_normal gt_bottom_border" style>Adjustments by GDP and PPP (US$)</th>
    </tr>
  </thead>
  <thead class="gt_col_headings">
    <tr>
      <th class="gt_col_heading gt_columns_bottom_border gt_center" rowspan="1" colspan="1">Global Income Group</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_right" rowspan="1" colspan="1">Conversion Factor</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_right" rowspan="1" colspan="1">Platinum</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_right" rowspan="1" colspan="1">Gold</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_right" rowspan="1" colspan="1">Silver</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_right" rowspan="1" colspan="1">Bronze</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_right" rowspan="1" colspan="1">Network</th>
    </tr>
  </thead>
  <tbody class="gt_table_body">
    <tr>
      <td class="gt_row gt_center">High income</td>
      <td class="gt_row gt_right">1.00</td>
      <td class="gt_row gt_right">$15,000</td>
      <td class="gt_row gt_right">$10,000</td>
      <td class="gt_row gt_right">$7,500</td>
      <td class="gt_row gt_right">$3,000</td>
      <td class="gt_row gt_right">$1,000</td>
    </tr>
    <tr>
      <td class="gt_row gt_center">Upper middle income</td>
      <td class="gt_row gt_right">0.34</td>
      <td class="gt_row gt_right">$5,100</td>
      <td class="gt_row gt_right">$3,400</td>
      <td class="gt_row gt_right">$2,550</td>
      <td class="gt_row gt_right">$1,020</td>
      <td class="gt_row gt_right">$340</td>
    </tr>
    <tr>
      <td class="gt_row gt_center">Lower middle income</td>
      <td class="gt_row gt_right">0.14</td>
      <td class="gt_row gt_right">$2,100</td>
      <td class="gt_row gt_right">$1,400</td>
      <td class="gt_row gt_right">$1,050</td>
      <td class="gt_row gt_right">$420</td>
      <td class="gt_row gt_right">$140</td>
    </tr>
    <tr>
      <td class="gt_row gt_center">Low income</td>
      <td class="gt_row gt_right">0.14</td>
      <td class="gt_row gt_right">$2,100</td>
      <td class="gt_row gt_right">$1,400</td>
      <td class="gt_row gt_right">$1,050</td>
      <td class="gt_row gt_right">$420</td>
      <td class="gt_row gt_right">$140</td>
    </tr>
  </tbody>
  
  
</table></div>

<br><br><br>
