+++
title = "Sponsoring Package Details"
description = "Want to support the R Stats Language for Statistical Computing and its world class community? Learn how to help and benefit from the reach of the useR! conference."
keywords = ["conference","sponsor","support","open source"]
+++

{{< cost_conversion "/cost_convert.html" >}}
