+++
title = "How the useR! 2021 Team Came Together"
date = "2020-06-24"
tags = ["R"]
author = "The UseR! 2021 Team"
categories = ["useR!"]

+++

Almost two weeks ago, we announced that useR! 2021 will be a global, virtual conference. As a result we are working together with the R Foundation to extend our the current team to become a truly global team. Time for us to look back, look back at how we found together as a team and how useR! 2021 came together.

## A Protocol

We created this blog to onboard you onto the journey to a global, virtual useR!.

*The below conversation was recorded on June 24th 2020 and was slightly redacted to be understandable for everyone. Muriel and Matthias have also been part of the team from the start but couldn't make the chat that day*


<h6 class = "chat-name">Corinna Grobe <span class = "chat-time">08:15 AM</span></h6>
<p>Good morning, everyone! How are you? It's early. Has everyone had their coffee yet?</p>

<h6 class = "chat-name">Rachel Heyard <span class = "chat-time">08:16 AM</span></h6>
<p>Good morning. I am at my second. So slowly getting there. 😜</p>

<h6 class = "chat-name">Jakob Dambon <span class = "chat-time">08:16 AM</span></h6>
<p>Hi everyone! ☕️ was delicious!</p>

<h6 class = "chat-name">Dorothea <span class = "chat-time">08:16 AM</span></h6>
<p>Good morning! All fine here (but I am currently attending a virtual conference and the "night session" was from 11pm to 2am so I am a little bit tired)</p>


<h6 class = "chat-name">Corinna Grobe <span class = "chat-time">08:18 AM</span></h6>
<p>I think @Christoph Sax and @Kirill Müller are not online yet.</p>

<h6 class = "chat-name"> <span class = "chat-time">08:18 AM</span></h6>
<p>But let's get going. They can join us later.</p>

<h6 class = "chat-name"> <span class = "chat-time">08:19 AM</span></h6>
<p>Thank you all for taking the time to be here today. Although “here” has become a very relative term over the last few months. However, I don't want to dwell on that too much right now. The reason I invited you today is that I just recently joined the team and of course missed the exciting early days. As I volunteered to work on communications, I should probably know how useR! 2021 Zurich came about. I am certain that you can help me out there and have some stories to tell 😉 </p>

<h6 class = "chat-name"> <span class = "chat-time">08:19 AM</span></h6>
<p>Let's go back to that time. I think you @Dorothea got the ball rolling…</p>

<h6 class = "chat-name">Dorothea <span class = "chat-time">08:20 AM</span></h6>
<p>I guess so. This is the first trace that I found in our Zurich R User Group Organizer Slack Space </p>

<div class = "mt-3 mb-5">
  <img src="/img/blog/tweet-1.jpg" style="width: 70%;">
</div>


<h6 class = "chat-name">Corinna Grobe <span class = "chat-time">08:21 AM</span></h6>
<p>Wow, that was way back in October 2019. What got you the idea?</p>

<h6 class = "chat-name">Dorothea <span class = "chat-time">08:21 AM</span></h6>
<p>The great thing was: I just launched this as a "speak out your crazy ideas" thing.</p>

<h6 class = "chat-name">Dorothea <span class = "chat-time">08:22 AM</span></h6>
<p>I saw the R Foundation's announcement for hosting useR! 2021 in a tweet (which was actually already two weeks old), and immediately thought, well....why not?</p>


<h6 class = "chat-name">Dorothea <span class = "chat-time">08:23 AM</span></h6>
<p>That all was only a few days before the deadline… But nevertheless,  members of the community joined immediately.</p>

<h6 class = "chat-name">Matt Bannert <span class = "chat-time">08:23 AM</span></h6>
<p>I agree. I think @Dorothea was the first to make the idea explicit. So if I had to take it down to one person it’s definitely her. But somehow it also came together in the community. Personally, for me it had a lot to do with attending useR! 2019 in Toulouse and going there by train with a group of Zurich #rstats people.</p>


<h6 class = "chat-name">Corinna Grobe <span class = "chat-time">08:24 AM</span></h6>
<p>As @Dorothea was saying, thumbs up and high fives came instantaneously, including from you @Rachel Heyard. You were immediately enthusiastic about the idea and volunteered?</p>

<h6 class = "chat-name">Rachel Heyard <span class = "chat-time">08:26 AM</span></h6>
<p>YES. 100% hooked. I remember that I was not so involved for the first proposal as it came so spontaneously and I had other commitments. I kept in close contact and got involved right after the busier period. 😄</p>

<h6 class = "chat-name">Jakob Dambon <span class = "chat-time">08:27 AM</span></h6>
<p>The same for me. At this time, I was working with @Dorothea. She mentioned her "crazy idea", which got me <a href = "https://twitter.com/JakobDambon/status/1186519988640858112">thinking</a>:</p>

<div class = "mt-3 mb-5">
  <img src="/img/blog/tweet-2.jpg" style="width: 40%;">
</div>


<h6 class = "chat-name">Corinna Grobe <span class = "chat-time">08:27 AM</span></h6>
<p>Ah yes, I saw that tweet. What was it that got you excited about a useR! 2021 in Zurich @Jakob Dambon?</p>

<h6 class = "chat-name">Jakob Dambon <span class = "chat-time">08:27 AM</span></h6>
<p>I attended most of the Zurich R User Group MeetUps, but I was not part of the organizing team. Zurich has a thriving R community, however, never held a useR! conference. That was the motivation. Personally, it was the best way to get more involved and be part of something "big".</p>

<h6 class = "chat-name">Dorothea <span class = "chat-time">08:27 AM</span></h6>
<p>Also, @Matt Bannert had given a talk about his Toulouse useR! experience in a MeetUp not long before that, and this was certainly a motivator to try to get useR! to Zurich. And his enthusiasm was crucial in pushing the project forward and writing the proposal. </p>

<h6 class = "chat-name">Matt Bannert <span class = "chat-time">08:28 AM</span></h6>
<p>Hahaha. Now, I remember that talk @Dorothea. Even found the slides <a href = "https://zhr-meetup-201910.netlify.app/">here</a>.</p>

<h6 class = "chat-name">Matt Bannert <span class = "chat-time">08:29 AM</span></h6>
<p>Attending Toulouse and meeting Rob Hyndman and Achim Zeileis in person was very inspiring to me. We met multiple times throughout the conference as we share common interests (time series and colorspaces) and were in the same session together. So there was plenty of time to talk and learn how they started. It was very inspiring to hear first hand from Achim about the early days of useR! and seeing what it had become over the years. I let that sink in over the weeks after the Toulouse event. By the time I was back in Zurich and digesting useR! 2019 there were already a lot of fruitful thoughts in the Zurich R community as @Rachel Heyard and @Dorothea described.</p>

<h6 class = "chat-name">Rachel Heyard <span class = "chat-time">08:29 AM</span></h6>
<p>Due to the Zurich R User Group there was already a relatively large group of crazy R passionates that were very well connected.
😄</p>

<h6 class = "chat-name">Matt Bannert <span class = "chat-time">08:30 AM</span></h6>
<p>Right @Rachel Heyard. I think what the talk also shows is the importance of like minded people thinking about the same at roughly the same time. I mean like going to Toulouse by train. This was also the first time we as a group were in touch with Achim over Twitter who came by train from Austria.</p>


<h6 class = "chat-name">Corinna Grobe <span class = "chat-time">08:30 AM</span></h6>
<p>Was it difficult to turn the crazy idea into a real mission with motivated people? Or was everyone motivated to work on the proposal - after all, you had only little time until the submission deadline?</p>

<h6 class = "chat-name">Dorothea <span class = "chat-time">08:33 AM</span></h6>
<p>Writing the proposal was amazing, a real group effort. Everyone contributed and advanced the proposal, after all we had less than two weeks time. It was also great that we quickly agreed on what would be important to us in a conference.</p>


<h6 class = "chat-name">Corinna Grobe <span class = "chat-time">08:34 AM</span></h6>
<p>Yes, that's right. The message of useR! 2021 is “Zurich is looking forward to hosting a planet-friendly, community-based, inclusive, and innovative useR! 2021.”  Why did you choose these very attributes?</p>

<h6 class = "chat-name">Matt Bannert <span class = "chat-time">08:37 AM</span></h6>
<p>I think the planet-friendliness is inevitable to me. I have never seen the point of travelling around the world for a 15-minute lecture - as is so often the case in the academic world. I was inspired by Heidi Seibold and Achim, who had discussed exactly this topic on Twitter and didn't fly once the whole year. I think this was one of the first common denominators of the group.</p>

<h6 class = "chat-name">Rachel Heyard <span class = "chat-time">08:38 AM</span></h6>
<p>I guess the message also represents us as a group best... we are all quite young, flexible and innovative. We want to shape the future of the R community as well as of conferences in general. The future is green and diverse. With useR! 2021 we see our chance of making a teeny-tiny difference and we'll make the best out of it. 😜 </p>

<h6 class = "chat-name">Jakob Dambon <span class = "chat-time">08:38 AM</span></h6>
<p>After all, the R Community is a mirror of the society in which it lives. And these attributes mirror Zurich and our LOT (Local Organization Team).</p>

<h6 class = "chat-name">Matt Bannert <span class = "chat-time">08:39 AM</span></h6>
<p>Do you see all of this agreement @Corinna Grobe? That’s why. 😉</p>


<h6 class = "chat-name">Corinna Grobe <span class = "chat-time">08:41 AM</span></h6>
<p>That’s fantastic. It is crucial to find like-minded people when embarking on such an adventure.</p>

<h6 class = "chat-name">Dorothea <span class = "chat-time">08:41 AM</span></h6>
<p>We are a very young team. So many of these attributes come naturally for us. Being planet-friendly is a no brainer, striving to be inclusive as well. And we all see things in conferences that we don't like, so organising our own useR! is the chance to be innovative 😄. And all this IS community-based because the idea came from the community.</p>

<h6 class = "chat-name">Matt Bannert <span class = "chat-time">08:41 AM</span></h6>
<p>@Corinna Grobe I think @Christoph Sax has arrived.</p>


<h6 class = "chat-name">Corinna Grobe <span class = "chat-time">08:41 AM</span></h6>
<p>Hi @Christoph Sax. Great to have you.</p>

<h6 class = "chat-name">Christoph Sax <span class = "chat-time">08:42 AM</span></h6>
<p>I have been here for a long time 😂</p>

<h6 class = "chat-name">Dorothea <span class = "chat-time">08:42 AM</span></h6>
<p>So @Christoph Sax is the one who masterfully combines stew cooking and adding the finishing touches to the proposal layout .</p>

<h6 class = "chat-name">Rachel Heyard <span class = "chat-time">08:43 AM</span></h6>
<p>@Christoph Sax is the layout-king.</p>


<h6 class = "chat-name">Corinna Grobe <span class = "chat-time">08:43 AM</span></h6>
<p>@Christoph Sax how do these two things go together? 🤔 </p>

<h6 class = "chat-name">Christoph Sax <span class = "chat-time">08:44 AM</span></h6>
<p>That's essentially the same. You just need to get the ingredients right.</p>

<h6 class = "chat-name">Dorothea <span class = "chat-time">08:45 AM</span></h6>
<p>Now I am hoping that I am not the only one remembering @Christoph Sax stew cooking during the second proposal writing (which happened between Christmas and New Year, all over Slack).</p>

<h6 class = "chat-name">Christoph Sax <span class = "chat-time">08:45 AM</span></h6>
<p>Unfortunately, I missed the early part of the proposal drawing, so I only had the chance to check-in later on.</p>


<h6 class = "chat-name">Corinna Grobe <span class = "chat-time">08:45 AM</span></h6>
<p>So will useR! 2021 have its very own Swiss layout?</p>

<h6 class = "chat-name">Christoph Sax <span class = "chat-time">08:46 AM</span></h6>
<p>Yes, useR! 2021 will have its own zurichdown template.</p>


<h6 class = "chat-name">Corinna Grobe <span class = "chat-time">08:47 AM</span></h6>
<p>zurichdown - I like the name. Can we already see it being used somewhere @Christoph Sax?</p>

<h6 class = "chat-name">Christoph Sax <span class = "chat-time">08:47 AM</span></h6>
<p>It is <a href = "https://gitlab.com/useR2021zrh/user2021zrh/-/tree/master/zurichdown">here</a>.
Not sure if it will be public later on.</p>



<h6 class = "chat-name">Corinna Grobe <span class = "chat-time">08:48 AM</span></h6>
<p>I've heard that parts of zurichdown will eventually be incorporated into the website...</p>

<h6 class = "chat-name">Christoph Sax <span class = "chat-time">08:48 AM</span></h6>
<p>Or the other way round. But we will try to develop a unified CI style over time.</p>


<h6 class = "chat-name">Corinna Grobe <span class = "chat-time">08:48 AM</span></h6>
<p>Exciting. I look forward to seeing that develop. </p>


<h6 class = "chat-name">Corinna Grobe <span class = "chat-time">08:49 AM</span></h6>
<p>Let's change the subject. Something else I wanted to touch on is Slack. In the past months, Slack has been the most important communication and organization channel for the LOT. It seems, however, that Slack was very central even before the conference organization started. Why is that?</p>

<h6 class = "chat-name">Dorothea <span class = "chat-time">08:49 AM</span></h6>
<p>We were already on Slack for the Zurich R User Group, so this was a natural choice.</p>

<h6 class = "chat-name">Matt Bannert <span class = "chat-time">08:50 AM</span></h6>
<p>Its asynchronous nature is very important. We’ve all got our jobs and need to do useR! in between things mostly. When you miss a meet or are late you can drop your two cents later on. Also in times of uncertainty it helped two form small groups to work on a particular task and form another group to work on another task. Like ad hoc teams for stuff until everything is more settled.</p>


<h6 class = "chat-name">Corinna Grobe <span class = "chat-time">08:53 AM</span></h6>
<p>Speaking of uncertainty: As soon as you had been awarded the contract by the R Foundation to host useR! 2021, the Corona pandemic came and many things were questioned. No easy time. How did you feel about that? What's kept you guys busy these past months?</p>

<h6 class = "chat-name">Matt Bannert <span class = "chat-time">08:54 AM</span></h6>
<p>Of course it was a hard hit. But pioneers like the eRum organizers have done a great job of paving the way. Our situation is different. We got a year to prepare. We have to adapt because there is no alternative. Better embrace adaptation.</p>

<h6 class = "chat-name">Christoph Sax <span class = "chat-time">08:56 AM</span></h6>
<p>And the Corona pandemic made all the conferences truly planet-friendly 😂</p>

<h6 class = "chat-name">Rachel Heyard <span class = "chat-time">08:56 AM</span></h6>
<p>It was also extremely nice to have this exact group of people to talk to every so often, and have something else than Corona (and something as nice as R) to think about sometimes. 🤓 There are some topics related to the content of the conference that we could already work on.</p>

<h6 class = "chat-name">Dorothea <span class = "chat-time">08:57 AM</span></h6>
<p>Yes that was challenging. For me, the realization that this would affect ”our” useR! came slowly. From the start of the uncertainty, we tried to keep working on what we could. We realized that we needed a "working hypothesis" to be able to continue, even if that was likely to change. So as @Rachel Heyard said, we started working on content rather than form.</p>

<h6 class = "chat-name">Rachel Heyard <span class = "chat-time">08:57 AM</span></h6>
<p>It also took me long to realize that this will affect our conference, @Dorothea. I was always thinking “right, our turn is only in 2021, it will all be back to normal by then.” </p>

<h6 class = "chat-name">Jakob Dambon <span class = "chat-time">08:57 AM</span></h6>
<p>Aligning our goals with the current situation is not easy, but now we can really think outside the box.</p>

<h6 class = "chat-name">Matt Bannert <span class = "chat-time">08:58 AM</span></h6>
<p>I mean we wanted to influence how conferences are being done as @Rachel Heyard pointed out earlier. Now the impact can even be bigger as more substantial changes / experiments are accepted more willingly by everyone. And also, Achim, who I really refer to as a mentor in this, told @Dorothea and me they tried out stuff in the early days. Some of it worked, some of it didn’t. So they came back the next year.</p>

<h6 class = "chat-name">Dorothea <span class = "chat-time">08:58 AM</span></h6>
<p>I found the open exchange with the R Foundation Conference Committee very rewarding. There are so many factors to consider when thinking about how useR! can take place in 2021.</p>

<h6 class = "chat-name">Dorothea <span class = "chat-time">09:00 AM</span></h6>
<p>A central point remains our claim to make it “planet-friendly, inclusive, innovative and community-based”.</p>

<h6 class = "chat-name">Matt Bannert <span class = "chat-time">09:01 AM</span></h6>
<p>Absolutely, I can only agree with @Dorothea regarding the exchange with the R Foundation Conference Committee. It has been a privilege to be able to discuss with them. The fact that the stars of the community actually listen and support us the way they do is truly special to this community and a big reason why we as a young team want to be part of useR as opposed to reinventing the wheel and starting our own. </p>


<h6 class = "chat-name">Corinna Grobe <span class = "chat-time">09:03 AM</span></h6>
<p>Would you dare and venture a prognosis at this point of time? Where are we headed with useR! 2021?</p>

<h6 class = "chat-name">Matt Bannert <span class = "chat-time">09:06 AM</span></h6>
<p>Tough question. We owe the pioneers of e-Rum and user2020 and many other conferences that had to hit the ground running when going virtual. When can benefit a lot from that experience. Going virtual is the best way for us to stick to our main pillars: a planet-friendly, inclusive and community driven conference. We might very well have very different travel restrictions across the globe in summer 2021. </p>
<p>Online fatigue will be the challenge. Experience and innovation are your panaceas to cure it. Going virtual is the best way to stay inclusive. We are reaching out to R enthusiasts across the globe and will very soon onboard new members onto our team. Speaking of the conference itself, we might still have smaller local events like debriefing parties or a social hike here in Switzerland among smaller groups that meet and discuss attending useR! together if this can be done responsibly.
</p>


<h6 class = "chat-name">Dorothea <span class = "chat-time">09:14 AM</span></h6>
<p>useR! is a global conference with people from very different backgrounds, so there is a great wealth of inspiration and inputs.</p>


<h6 class = "chat-name">Corinna Grobe <span class = "chat-time">09:15 AM</span></h6>
<p>Time flies. @Dorothea and @Rachel Heyard have to go on to the next meeting. I thank you all for your time. Thanks to you I now understand much better what useR! 2021 Zurich is and what it is all about.</p>

<h6 class = "chat-name">Christoph Sax <span class = "chat-time">09:16 AM</span></h6>
<p>Thanks to you for all the great work!</p>

<h6 class = "chat-name">Jakob Dambon <span class = "chat-time">09:16 AM</span></h6>
<p>Thanks everyone. Have a good one!</p>

<h6 class = "chat-name">Rachel Heyard <span class = "chat-time">09:16 AM</span></h6>
<p>Thank you all and happy Wednesday. I hope you'll enjoy the sun a bit. 🙂</p>

<h6 class = "chat-name">Matt Bannert <span class = "chat-time">09:16 AM</span></h6>
<p>Thanks @Corinna Grobe.</p>

<h6 class = "chat-name"> Dorothea <span class = "chat-time">09:16 AM</span></h6>
<p>Thanks everyone!</p>


