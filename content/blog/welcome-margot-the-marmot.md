+++
title = "Welcome Margot, the Marmot!"
date = "2020-11-26"
tags = ["Community"]
author = "The useR! 2021 Team"
categories = ["useR!"]
banner = "/img/artwork/marmot-mir.png"

+++

# useR! 2021 Key Visual

<div class="imgpost">
   <img src="/img/artwork/marmot-mir.png" width="100">
   <img src="/img/artwork/marmot-rladies.png" width="100">
   <img src="/img/artwork/marmot-africar.png" width="100">
   <img src="/img/artwork/marmot-div.png" width="100">
   <div class="postcaption">From MiR to R-Ladies and AfricaR, Margot got 'em all – fashionable scarfs.</div>
</div>


#RStats world, please welcome our latest addition to the team. Say hi to Margot, the Marmot. 
Margot is a cosmopolitan gnawer with roots around the globe. She spent the early days of her life 
in Northern Africa and Europe. 

<div class="imgpost">
   <img src="/img/blog/idea.png" width="450px">
   <div class="postcaption">The idea to replace the traditional pictures of locations in useR! material with an animal was first brought up by Fodil in our Slack space.</div>
</div>

It became clear quickly though that an animal who is supposed to represent our global conference needed to cross the big pond. So her foster fathers made sure she was able to communicate was she was all about on the other side of the Atlantic Ocean. They squeezed her into a wire and sent her off to Argentina to grow up. 

<div class="imgpost">
   <img src="/img/blog/sketch1.png" width="450px">
   <div class="postcaption">Matt, drew up a first sketch in order to put the idea of a Marmot in an internationally understood message.</div>
</div>

In Argentina, Margot became a pretty, young Marmot lady who started to build character. Thanks to her innate curiosity, her shrewd thinking, quick paws and cheerful, happy personality the team adopted her quickly as a key visual and mascot for the 2021 conference. 

<div class="imgpost">
   <img src="/img/blog/pretty.png" width="450px">
   <div class="postcaption">Argentinian artist/illustrator Francisco Etchart brought the Marmot to life.</div>
</div>

As of today, Margot the Marmot is all over the useR! Slack space from Argentina and the U.S. to Australia, Asia, Africa, Europe and back again. When she is not on the lookout or gone hiking in the mountains, she enjoys a good bag of popcorn in front of a computer screen learning #RStats. Plus, Margot has a weakness for fashionable scarfs. Word on our team is, she loves Yoga. Some team members even claim to have spotted her doing Yoga poses...  






<br>
<br>