+++
title = "Program Overview"
description = "What's happening at the event"
keywords = ["program","schedule","events","topics"]
+++


useR! 2021 will take place **5-9 July 2021**. Monday, Tuesday, Thursday and Friday are the main conference days. On Wednesday, you can "take a break" from the conference at one of our tutorials, participate in one of the satellite events or simply enjoy some time away from your screen.

Scheduling a global conference is not an easy task. We decided to favour one timezone each day: PDT on Monday, CEST on Tuesday, AWST on Thursday and CEST again on Friday (Friday is subject to change, depending on submissions that we get). Tutorials on Wednesday will be in different timezones throughout the day. 
The main conference days feature keynotes to look at the big picture, contributed sessions where everyone should find their specialisation, panel discussion on hot topics, incubators to work together on emerging questions, Elevator pitches for extended exchange on new research and social events for networking. Do you want to know more about these format? <a href="https://user2021.r-project.org/program/formats">Have a look at our formats description!</a>

**The schedule below gives you a broad overview of the conference, so that you know when to block off time for useR! 2021.** 
We are looking forward to an exciting and inspiring week of R! 

{{< calendar "https://benubah.github.io/rcentral/useR/calendar.html" >}}


<br><br><br>
