+++
title = "Accessibility"
description = "Guidelines and standards to make useR! 2021 accessible."
keywords = ["Tutorials","Accessibility"]
+++

# Accessibility at useR! 2021

The need for accessibility is recognized but still too often not actually enforced or put at the center of the design of a conference. The onset of the COVID-19 pandemic forced everyone shift the ways we work and meet. As many events that were supposed to be held in place, useR! 2021 had to make the shift towards an online conference. We are looking for ways to make this experience an opportunity to implement accessibility standards that last after the pandemic ends. Our goal at UseR! 2021 is to set up the bar high regarding accessibility. We do as much as we can as organizers, and we expect presenters to be mindfull of accessibility in their contributions. 

We have a specific contact e-mail address for questions, feedbacks, or comments: [user2021-accessibility[at]r-project.org](mailto:user2021-accessibility@r-project.org).

## Our Efforts as Organizers

+ The technology team is working to ensure the conference platform will be accessible with screen reading software and easy to navigate.
+ Abstract and Tutorial submission, as well as Registration forms are designed with accessibility in mind.
+ Recorded content will be captioned.
+ Live content will be captioned live whenever this is possible. 
+ We strive to make all conference materials (e.g., schedule, abstracts, slides) available in accessible formats before the conference.
+ We define accessibility guidelines and remind our contributers to adhere to these.

## Accessibility Guidelines
In spite of the awareness that accessibility is a key step towards inclusion, sometimes we do not know how to improve our presentations, scripts, and figures. For this reason, we provide these accessibility guidelines for participants to adhere to when making their contributions. Please read our blog post for more details. The text below gives you indications that are valid for most contributions. We have also prepared short checklists that are specific for each format: 

+ [Regular Talks and Lightning Talks](/participation/talks-access)
+ [Technical Notes](/participation/technical-access)
+ [Panels and Incubators](/participation/panels-access)

For specific questions regarding accessibility, you can also contact us via email at [user2021-accessibility[at]r-project.org](mailto:user2021-accessibility@r-project.org) .

### Slides

+ Text-based slides, such as rmarkdown, beamer or xaringan are highly recommended because screen readers can read them or the text files that structure them.
+ UseR! 2021 has a <a href="https://github.com/yihui/xaringan" target="_blank">__xaringan__</a> template to guide you. See [Creating xaringan slides using the useR! theme](#creating-xaringan-slides-using-the-user-theme).
+ Give preference to slides that can be hosted and navigated on the web. These can be scrolled independently by the audience while you present your talk.
+ If you need to present in MS PowerPoint or similar, keep the original file and avoid providing only a PDF to your audience. Semantic formatting (e.g., nested lists) is often unavailable to scree readers in the .pptx format. 

### Slide availability

We want to make your slides available before your talk, so that participants can have access to the content while you present. Place your slides in a web-based platform, such as Github pages, GitLab pages, RPubs, or Google Slides. Having access to the material will not only improve accessibility for blind or low-vision participants, but help everyone at the conference to follow what is being presented.


### Slide appearance and format tips

+ Create simple slides without complicated layouts, formatting, or animations and transitions.
+ Provide alt-text for all your figures, particularly if you use image-only slides.
+ Make sure color palettes can be seen by people with color blindness.
+ Check the font size and present from your smallest screen. Avoid small fonts.
+ Prefer high contrast <!--+ Prefer light text on dark background-->.
+ Add alt-text to your plots and figures and describe them briefly but completely when presenting them, highlighting the essential parts of the plot according to the context of the presentation. For tutorials, include a longer explanation of how the code relates to the visual output. 

### Other reminders for oral presentation and Q&A sessions

+ Speak as clearly as possible, looking at the camera, and try not to go too fast.
+ Mention slide headers or numbers when switching slides.
+ Comment briefly on what it is that you are showing and why. Avoid explanations that rely only on obvious features that may not be obvious for everyone, such as "As you can see" or "The image speaks for itself".



### Posting in social media 

If you create social media posts before and during the conference, please limit your use of emojis and capitalize the words inside hashtags ex. `#WeAreUseR`. Remember to include descriptive text with your images. For example, Twitter has an option to include alt-text when posting an image. 


## Other considerations 

At useR! 2021 we aim to welcome people from around the globe. This includes a large majority of people who are non-native speakers of English. We would like to make their conferene experience as comfortable as possible. Captions should help to follow presentations more easily. Regular talks and Lightning presentations can be pre-recorded in other languages than English, as long as an English transcript is provided for subtitles. __captioning__ and __translation__ are one key to connect all the conference attendants. Steps towards the inclusion of people with disabilities are beneficial for everyone.  


### Creating xaringan slides using the useR! theme

+ Install __xaringan__ package from GitHub `remotes::install_github('yihui/xaringan')`
+ If you are using RStudio:
  + Go to `File > New file > RMarkdown`
  + Select __From template__: Ninja presentation. 
+ If you do not use RStudio you can create a .Rmd file and add a xaringan YAML header:

      ```
      ---
      title: "A Cool Presentation"
      output:
        xaringan::moon_reader:
      ---
      ```

+ To use the useR! theme, add the following CSS options in the YAML header of the presentation:

      ```
      output:
        xaringan::moon_reader:
          css: ["useR", "useR-fonts"]
      ```

+ To knit the document, click on the knit button in RStudio, or use `rmarkdown::render()`. Alternatively, you can knit a live preview using `xaringan::inf_mr()`

## Additional references 

 <a href="http://www.sigaccess.org/welcome-to-sigaccess/resources/accessible-conference-guide/#online" target="_blank">Accessible Conference Guide</a>

 <a href="https://osf.io/k3bfn/" target="_blank">Enhancing the inclusivity and accessibility of your online calls</a>

 <a href="https://www.w3.org/WAI/teach-advocate/accessible-presentations/" target="_blank">How to Make Your Presentations Accessible to All</a>

<a href="https://www.metadocencia.org/post/accesibilidad_1/" target="_blank">Cómo hacer reuniones virtuales pensadas para todas las personas (How to make virtual meetings designed for everyone)</a>

<a href="http://webaim.org/resources/contrastchecker/" target="_blank">Contrast checker</a>.  


Mohamed El Fodil Ihaddaden's package <a href= "https://github.com/feddelegrand7/savonliquide" target="_blank"> 'savonliquide'</a> for an API in R

<a href="https://www.techsmith.com/blog/how-to-create-alternative-text-for-images-for-accessibility-and-seo/" target="_blank">How to Create Alternative Text for Images for Accessibility and SEO</a>

<a href="https://blog.hootsuite.com/inclusive-design-social-media/" target="_blank">Inclusive design social media<\a>

<a href= "https://www.wgbh.org/foundation/ncam/guidelines/guidelines-for-describing-stem-images" target="_blank"> WGBH - Guidelines for Describing STEM Images</a>

<a href= "http://diagramcenter.org/making-images-accessible.html" target="_blank">Diagram Center (Accessible Graphics)</a>

<a href= "http://diagramcenter.org/accessible-math-tools-tips-and-training.html" target="_blank">Diagram Center (Accessible Math)</a>

<a href= "https://www.csun.edu/universal-design-center/math-and-science-accessibility#A51" target= "_blank">CSUN - 
Math and Science Accessibility Overview</a>


<br><br><br>
