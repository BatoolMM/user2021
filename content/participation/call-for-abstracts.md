+++
title = "Call for Abstracts"
description = "Want to share your work and contribute to useR! 2021? Submit an abstract. "
keywords = ["abstract","presentation"]
+++

## Call for Abstracts

**<a href="https://graphs.kof.ethz.ch/user-abstracts">Abstract Submission Form</a>**

useR! 2021 invites R users and developers from around the world to submit abstracts to contribute to our global virtual conference. We expect a wide range of exciting talks, innovative elevator pitches, horizon-expanding discussion panels, and productive incubators. 
This is an opportunity to share your work with international colleagues who are R enthusiasts, including many current and future leaders in the R community. We welcome abstracts related to R from any disciplinary background, whether the work is computational, empirical, or theoretical in nature. Investigators at all career stages are encouraged to submit their work.

Abstracts will be peer-reviewed. Acceptance will be based on content, available space, and overall program balance. At least one author of each accepted abstract will be expected to register for the conference and attend the session in which their presentation is scheduled (during July 5–9, 2021). At most one abstract will be accepted per presenter. People presenting on behalf of a community organization may additionally present their own work. 

Abstracts for the following formats are invited: <br>

- *Regular talks* are the backbone of useR! conferences, with high-quality contributions from different fields. The talks will be limited to 15 minutes and will be followed by a short discussion. The talks can be either live or pre-recorded, but if they are pre-recorded the presenters should still be present for the live Q&A. 
- *Elevator pitches* replace poster sessions. If your abstract is accepted, you can decide whether you want to present your project as a technical note or as a pre-recorded lightning talk of 4–5 minutes. Presenters will attend their designated elevator pitch session for Q&A and networking.
- *Panels* are meant for discussions of topics of broad interest. Typically, a few experts are invited but the audience can be involved as well.
- *Incubators* give the opportunity to people working in similar fields to connect and work on a common goal or question.


useR! 2021 aims to be a global conference. We want to give you the opportunity to present your regular talk or lightning talk in your own language, if you feel more comfortable doing so. This would mean that your presentation needs to be pre-recorded and an English transcript for subtitles needs to be provided. For the review process, you would still need to provide an English abstract. You may provide a translation of the abstract in your own language after acceptance of your contribution. 

Details about the formats are provided below. Please make sure to read the instructions carefully. We have prepared accessibility guidelines for all formats. You are invited to read and accept them before submitting your abstract. 

Abstract submission is open from **Monday, January 25th** to **Monday, March 15th**

**<a href="https://graphs.kof.ethz.ch/user-abstracts">Abstract Submission Form</a>**

## Format details

**Regular talks** <br>
Regular talks  will be limited to 15 minutes. Please consider the following guidelines when submitting an abstract for a regular talk: <br>

* Talks should be new to useR! 2021. If a talk was presented at a previous useR! conference or at a major international conference, then we will judge the abstract on the added contribution since the previous talk.
* Talks should be original and directly related to the speaker’s work. Talks discussing or surveying the work/packages of others may be considered for lightning talks or posters, but not generally for oral presentations.
* Successful submissions for oral presentations are typically backed by an active repository (in the case of a package), a technical report, a preprint, or an accepted manuscript at the time of review. If this does not apply to your work, please consider submitting an elevator pitch.
* Novel case studies are typically better adapted for the elevator pitch format.
* Your presentation should be directly related to R. General data science talks are not typically appropriate for regular talks, though some exceptions may be made.

**Elevator pitches** <br>
Elevator pitches can be presented in two different forms. Lightning talks are 5 minutes long, and are a great way to advertise your work! Technical notes should contain some text and graphs like a poster, be short and readable in 5 minutes. We will provide a template for the layout. You don’t have to specify the exact format at the stage of submission but can decide upon acceptance of your contribution. 
This is where any work related to R that may not quite fit in the regular talk category is fair game. We would love to see creative and interesting ideas. Elevator pitch sessions will be scheduled throughout the conference to allow participants to explore these contributions and directly interact with presenters. The presenters of a specific elevator pitch session will be asked to be available during designated time slots for Q&A and networking. The Q&A sessions will be hosted on the conference platform. 

**Panels** <br>
Panels are suitable for topics that are of interest to a large audience. The general idea is to prepare a 60-minute topic discussion with experts/stakeholders. There can be some form of interaction with the audience (e.g., a moderated Q&A). In your abstract, you should give an overview of the topic and identify open questions that you would like to discuss with your invitees. You can use the additional field in the form to explain who you would like to invite and how you plan to make your panel participative and relevant for the useR! audience.

**Incubators** <br>
Incubators address specific topics that are of interest to a smaller group of people working in the same field. They should have a specific aim (e.g., formulate ten principles for good XYZ) and not be mere networking sessions. In your abstract, you should identify the general topic and the specific aim of your 60-minute incubator. In the additional text field, you should explain how you will facilitate the achievement of the aim, how collaboration between participants of your incubator is envisaged, if you plan to invite an expert on the topic, etc. Also, include what you would consider tangible outcomes for your incubator session.

## Community Support

useR! 2021 would like to support underrepresented groups interested in speaking at this year’s conference. Several community groups have processes in place for presenters to receive feedback on their abstracts before applying. If you identify with any of these communities, please reach out to them for support and feedback!

If you would be willing to volunteer to give feedback through any of these organizations, please contact them as noted below. Your support is appreciated!

In alphabetical order:

**AfricaR:**<br>
Send your Abstract to africarusers[at]gmail.com.<br>

**LatinR:**<br>
Please join our slack space  <a href="https://join.slack.com/t/latin-r/shared_invite/zt-572yh8zr-h~wl5wtgycVA6FqWYCCg_w" target="_blank" >slack space </a> - in the #clinicadecharlas members of the community will give each other informal feedback on abstracts to be submitted. <br>

**MiR:**<br>
Submit abstract requests here: <a href="https://forms.gle/KTtiSoE32K6P2X2o6" target="_blank" >Link to Form</a><br>

**R-Ladies Global:**<br>
Submit requests for 1:1 feedback via this form: <a href="https://tinyurl.com/rladiesabstracts" target="_blank">Link to form</a> <br>
Volunteer to review via this form:  <a href="https://tinyurl.com/rladiesrevs" target="_blank">Link to form</a><br>
New!: Join the <a href="https://rladies-community-slack.herokuapp.com/" target="_blank">R-Ladies Community Slack</a> #UseR2021-community-feedback channel for faster feedback (or to help review others’ abstracts), if you are comfortable!<br>

If your R Community is not listed and would like to be listed - contact us!


## Submission guidelines

Please read the [code of conduct](https://user2021.r-project.org/participation/coc/) and the accessibility guidelines for your format ([regular talks](https://user2021.r-project.org/participation/talks-access/), [elevator pitches](https://user2021.r-project.org/participation/technical-access/), [panels and incubators](https://user2021.r-project.org/participation/panels-access/)) before submitting the abstract. 

A complete abstract submission requires:

* Information about you and your co-authors, along with affiliations
* A title
* A primary topic --- see list below for possible areas.
* 3–5 keywords.
* An abstract of up to 250 words (2500 Characters).
* For regular talks, we strongly encourage you to link your submission to a GitHub (GitLab, Bitbucket, or other) repo, a technical report, a preprint, or an accepted paper.
* For lightning talks and technical notes, a link to an external resource is recommended though only optional.
* For panels and incubators, please use the additional text field to give details on the organization of your contribution (see sections above on ‘Panels’ and ‘Incubators’)
* An indication of whether or not you presented the submitted topic at a previous international R conference, and if it has been published elsewhere. 
* You need to accept our CoC and read the accessibility guidelines for your chosen format

**List of topics** <br>
Community and Outreach <br> 
Teaching R/R in Teaching <br>
Big / High dimensional data <br>
Data mining, Machine learning, Deep Learning and AI <br>
Databases / Data management <br>
Statistical models <br>
Mathematical models <br>
Bayesian models <br>
Multivariate analysis <br>
Time series <br>
Spatial analysis <br>
Data visualisation <br>
Web Applications (Shiny, Dashboards) <br>
Efficient programming <br>
R in production <br>
Reproducibility <br>
Interfaces with other programming languages (C++, Ruby, Python, JS among others) <br>
Bioinformatics / Biomedical or health informatics <br>
Biostatistics / Epidemiology <br>
Economics / Finance / Insurance <br>
Social sciences <br>
Environmental sciences <br>
Ecology <br>
Operational research and optimization <br> 
R in the wild (Applications of R that are unusual and rare) <br>
Other <br>

**<a href="https://graphs.kof.ethz.ch/user-abstracts">Abstract Submission Form</a>**

<br><br><br>
