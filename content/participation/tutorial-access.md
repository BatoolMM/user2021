+++
title = "Accessibility Standards for useR! 2021 Tutorials"
description = "Guidelines and standards to make tutorials accessible."
keywords = ["Tutorials","Accessibility"]
+++

For tutorials, we kindly ask that prospective instructors: 

## When Preparing the Tutorial Material

- Give priority to text-based slide platforms such as markdown or Beamer
- Avoid transitions, animations, and complicated layouts
- Add alt-text to images that explain shortly but completely the features in the image. Include explanations of how the code relates to the visual output. 
- If you still need to present in MS PowerPoint or similar, keep the original file
- Add speaker notes to your slides

## While Giving the Tutorial

- Speak as clearly as possible, looking at the camera, and try not to go too fast
- Be aware of clues in the chat that signal if your pace is letting everyone follow your instructions 
- During the session, comment briefly on what it is that you are showing and why. Avoid explanations that rely only on obvious features that may not be obvious for everyone, such as "As you can see" or "The image speaks for itself"

For more information contact [user2021-accessibility [ at ]r-project.org](mailto:user2021-accessibility@r-project.org)
