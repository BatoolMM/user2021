+++
title = "Registration"
description = "Infos about Registration and Conference fees"
keywords = ["Fees","Registration"]
+++
# Registration
+ Conference registration will be open from mid April until mid June. 
+ Early bird fees apply until May 15th. 

# Conference Fees

Even though this is a virtual conference, costs are associated with it. Our fee system was selected considering two important points: 

- We want everyone to be able to participate! 
- We appreciate if you pay what you can! 

Below, you find suggested fees, for early bird and regular registration. The fees are adapted to the country where you live. Additionally, fees are waived if your employer doesn't have funds to cover the conference fees. There is no need to convince anyone, just tell us that you don't have the money. The _Academia_ rate applies also to non-profit organizations and government employees and the "student" rate applies also to retired people. Freelancers are encouraged to select the rate they feel applies to them.

If you are from a Low Income country, your fees are automatically waived. 

## Early Bird Fee  
{{<table "table table-striped table-bordered">}}
|          | High Income Country | Higher Middle Income Country | Lower Middle Income Country | Low Income Country |
|----------|---------------------|------------------------------|-----------------------------|--------------------|
| Industry | $75.00              | $25.50                       | $10.50                      | waived             |
| Academia | $50.00              | $17.00                       | $7.00                       | waived             |
| Student  | $25.00              | $8.50                        | $3.50                       | waived             |
{{</table>}}
<br>
## Regular Fee 
{{<table "table table-striped table-bordered">}}
|          | High Income Country | Higher Middle Income Country | Lower Middle Income Country | Low Income Country |
|----------|---------------------|------------------------------|-----------------------------|--------------------|    
| Industry | $100.00             | $ 34.00                      | $ 14.00                     | waived             |
| Academia | $75.00              | $25.50                       | $10.50                      | waived             |
| Student  | $50.00              | $17.00                       | $7.00                       | waived             |
{{</table>}}<br><br><br>
